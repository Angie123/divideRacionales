/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package divideracionales;

/**
 *
 * @author Angie Reyes
 */
public class DivideRacionales {

    //Variable que representa el numerador.
    int p;
    //Variable que representa el denominador 
    int q;
    
    /*
    *Constructor por defaault (Sin parámetros).
    */
    DivideRacionales(){
        this.p=0;
        this.q=0;
    }
    /*
    *Constructor por parámetros
    */
    DivideRacionales (int u, int z){
        this.p=u;
        this.q=z;
    }
    /**
     * Método que asigna un nuevo valor al numerador.
     * @param u El nuevo valor del numerador. 
     */
    private void setNumerador(int u){
        this.p=u;
    }
    /**
     * Método que asigna un nuevo valor al denominador.
     * @param z El nuevo valor del denominador. 
     */
    private void setDenominador(int z){
        this.q=z;
    }
    /**
     *Método que regresa el valor del numerador.
     * @return El valor del numerador. 
     */
    private int getNumerador(){
        return(this.p);
    }
    /**
     * Método que regresa el valor del denominador.
     * @return El valor del denominador
     */
    private int getDenominador(){
        return(this.q);
    }
    /**
     * Método que imprime el numero racional.
     */
    void show (){
        System.out.println(getNumerador()+ "/" + getDenominador());
    }
    /**
     * Método que realiza la división de dos números Racionales.
     * @param w El número racional con el que dividiremos.
     * @return El resultado de la división.
     */
    
    DivideRacionales divideRacionales(DivideRacionales w){
        int p=0;
        int q=1;
       p=getNumerador()*w.getDenominador();
       q=getDenominador()*w.getNumerador();
       return(new DivideRacionales(p,q));
    }
    /**
     * @param args 
     */
    public static void main(String[] args) {
        // Creamos dos objetos que representaran a dos numeros racionales.
        DivideRacionales w1= new DivideRacionales (1,2);
        DivideRacionales w2= new DivideRacionales ( 2,3);
        
        //Declaramos un objeto
        DivideRacionales w3;
        //Imprimimmos w1:
        System.out.print("W1 es: ");
        w1.show();
        //Imprimimos w2:
        System.out.print ("W2 es:");
        w2.show();
        //En w3 guardamos el resultado.
        //DIVISIÓN
        w3=w1.divideRacionales(w2);
        //Emprimimos el resultado.
        System.out.print("El resultado de la división es:");
        w3.show();
        
    }
    
}
